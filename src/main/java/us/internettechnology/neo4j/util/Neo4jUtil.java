package us.internettechnology.neo4j.util;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.base.Strings.nullToEmpty;
import static java.lang.System.getProperty;
import static java.nio.file.Paths.get;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.of;

/**
 * Utility methods to work with configuration.
 */
@Component
public class Neo4jUtil {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(Neo4jUtil.class);


    private final String neo4jEmbeddedWorkingDir;

    private final String neo4jEmbeddedWorkingSubdir;

    private final String neo4jEmbeddedConfigFileLocation;

    @Autowired
    public Neo4jUtil(@Value("${neo4j.embedded.working.dir:''}") String workingDir,
                     @Value("${neo4j.embedded.working.subdir:''}") String workingSubDir,
                     @Value("${neo4j.embedded.config.file.location:''}") String neo4jConfigFileLocation) {
        this.neo4jEmbeddedWorkingDir = nullToEmpty(workingDir).trim();
        this.neo4jEmbeddedWorkingSubdir = nullToEmpty(workingSubDir).trim();
        this.neo4jEmbeddedConfigFileLocation = neo4jConfigFileLocation;
    }


    public Optional<File> getNeo4jConfigFile(final File neo4jWorkingDirectory) {
        File configFile = null;
        if (isNullOrEmpty(neo4jEmbeddedConfigFileLocation)) {
            for (final File confDir : of(neo4jWorkingDirectory, neo4jWorkingDirectory.getParentFile())
                    .collect(toList())) {
                final File possibleConfigFile = getConfigurationFile(confDir);
                if (possibleConfigFile.exists()) {
                    configFile = possibleConfigFile;
                    break;
                }
                log.info("Can't find configuration file by path {}", possibleConfigFile.getAbsoluteFile());
            }
        } else {
            configFile = new File(neo4jEmbeddedConfigFileLocation);

        }
        if (configFile == null || !configFile.exists()) {
            log.info("neo4j.conf file doesn't exists . Default configuration will be created");
            configFile = copyDefaultConfig(getConfigurationFile(neo4jWorkingDirectory));
        }
        log.info("Neo4j config file location is {}", configFile.getAbsolutePath());


        return Optional.ofNullable(configFile);
    }

    private File getConfigurationFile(File neo4jWorkingDirectory) {
        return get(neo4jWorkingDirectory.getAbsolutePath(),
                getProperty("neo4j.embedded.conf.dir", "conf"),
                getProperty("neo4j.embedded.conf.main.file", "neo4j.conf")).toFile();
    }

    private File copyDefaultConfig(File destinationFilePath) {
        try {
            final ClassPathResource resource = new ClassPathResource("conf/neo4j.default.conf");
            Files.copy(resource.getInputStream(), get(destinationFilePath.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            log.error("Couldn't copy default config {} from classpath into {}");
            log.error(e.getMessage(), e);
        }
        return destinationFilePath;

    }

    public File getNeo4jWorkingDir() {
        String homeDirPath = neo4jEmbeddedWorkingDir;
        if (isNullOrEmpty(homeDirPath)) {
            homeDirPath = getProperty("catalina.home", getProperty("jetty.home"));
            if (homeDirPath == null) {
                throw new RuntimeException("Can't determine neo4j working directory path . " +
                        "Either set neo4j.embedded.working.dir property in configuration or run it on tomcat/jetty containers");
            }
        }
        final File workingDir = new File(homeDirPath, neo4jEmbeddedWorkingSubdir);

        if (!workingDir.exists()) {
            log.warn("Neo4j working directory {} doesn't exists . Try to create it", workingDir.getAbsolutePath());
            workingDir.mkdirs();
        }
        return workingDir;
    }
}