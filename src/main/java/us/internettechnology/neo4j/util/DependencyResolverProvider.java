package us.internettechnology.neo4j.util;

import org.neo4j.server.database.InjectableProvider;
import org.neo4j.server.rest.dbms.AuthorizationDisabledFilter;

import javax.servlet.Filter;
import java.util.Collection;

public enum DependencyResolverProvider {

    INSTANCE;

    private Collection<InjectableProvider<?>> singletons;

    private Filter authorizationFilter = new AuthorizationDisabledFilter();


    public Collection<InjectableProvider<?>> getSingletons() {
        return singletons;
    }

    public void setSingletons(Collection<InjectableProvider<?>> singletons) {
        this.singletons = singletons;
    }

    public Filter getAuthorizationFilter() {
        return authorizationFilter;
    }

    public void setAuthorizationFilter(Filter authorizationFilter) {
        this.authorizationFilter = authorizationFilter;
    }
}