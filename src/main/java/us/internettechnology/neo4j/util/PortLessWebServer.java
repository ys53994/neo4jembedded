package us.internettechnology.neo4j.util;

import org.neo4j.kernel.configuration.Config;
import org.neo4j.logging.LogProvider;
import org.neo4j.server.web.Jetty9WebServer;

import java.net.InetSocketAddress;

public class PortLessWebServer extends Jetty9WebServer {

    public PortLessWebServer(LogProvider logProvider, Config config) {
        super(logProvider, config);
    }

    @Override
    public void start() throws Exception {
        //Do nothing because we expose rest api via current application
    }

    @Override
    public InetSocketAddress getLocalHttpAddress() {
        return new InetSocketAddress("localhost",9999);
    }

    @Override
    public InetSocketAddress getLocalHttpsAddress() {
        return new InetSocketAddress("localhost",9998);
    }
}