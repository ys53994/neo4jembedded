package us.internettechnology.neo4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class Neo4jApplication extends SpringBootServletInitializer  {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Neo4jApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Neo4jApplication.class);
    }


}
