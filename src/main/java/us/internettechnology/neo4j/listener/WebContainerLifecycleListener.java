package us.internettechnology.neo4j.listener;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import us.internettechnology.neo4j.launcher.Neo4jLauncher;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import static org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext;

@WebListener
public class WebContainerLifecycleListener implements ServletContextListener {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(WebContainerLifecycleListener.class);

    @Autowired
    private Neo4jLauncher launcher;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        log.info("Web context has been initialized");
        autowireInnerComponents(event);
        launcher.start();
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        log.info("Web context has been destroyed {}", launcher);
        autowireInnerComponents(event);
        launcher.stop();
    }

    private void autowireInnerComponents(ServletContextEvent event) {
        if (launcher == null) {
            getRequiredWebApplicationContext(event.getServletContext())
                    .getAutowireCapableBeanFactory()
                    .autowireBean(this);
        }
    }

}