package us.internettechnology.neo4j.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.internettechnology.neo4j.launcher.Neo4jLauncher;

import java.util.Map;

/**
 * Helps manage neo4j embedded instance. In case when you want to stop|start neo4j without restarting whole servlet container
 * Can be disabled via configuration just set neo4j.embedded.management.controller.enabled=false if it is not required
 */
@ConditionalOnProperty(value = "neo4j.embedded.management.controller.enabled")
@RestController
@RequestMapping("/management")
public class Neo4jManagementController {

    private final Neo4jLauncher launcher;

    @Autowired
    public Neo4jManagementController(Neo4jLauncher launcher) {
        this.launcher = launcher;
    }

    @GetMapping("/restart")
    public String restart() {
        launcher.stop();
        launcher.start();
        return "Neo4j server has been restarted";
    }

    @GetMapping("/status")
    public String status() {
        return String.format("Running : %s", String.valueOf(launcher.isRunning()));
    }

    @GetMapping("/stop")
    public String stop() {
        if (!launcher.isRunning()) {
            return "Neo4j server is not running";
        }
        launcher.stop();
        return "Neo4j server has been stopped successfully";
    }

    @GetMapping("/start")
    public String start() {
        if (launcher.isRunning()) {
            return "Neo4j server is running";
        }
        launcher.start();
        return "Neo4j server has been successfully started";

    }

    @GetMapping("/configuration")
    public Map<String, String> getConfiguration() {
        return launcher.getConfig();
    }


}