package us.internettechnology.neo4j.configuration;

import org.neo4j.server.rest.dbms.AuthorizationDisabledFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import us.internettechnology.neo4j.launcher.Neo4jServletContainerBootstrapper;
import us.internettechnology.neo4j.util.DependencyResolverProvider;

import javax.servlet.*;
import java.io.IOException;

/**
 * Configuration .
 */
@Configuration
public class EmbeddedNeo4jConfig {

    @Bean
    public Neo4jServletContainerBootstrapper neo4jTomcatBootstrapper() {
        return new Neo4jServletContainerBootstrapper();
    }

    @Bean
    public FilterRegistrationBean neo4jAuthRegistrationBean() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new WrapperFilter());
        registration.addUrlPatterns("/*");
        registration.setName("authFilter");
        registration.setOrder(1);
        return registration;
    }


    private class WrapperFilter implements Filter {
        @Override
        public void init(FilterConfig filterConfig) throws ServletException {
            DependencyResolverProvider.INSTANCE.getAuthorizationFilter().init(filterConfig);
        }

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
            DependencyResolverProvider.INSTANCE.getAuthorizationFilter().doFilter(servletRequest, servletResponse, filterChain);
        }

        @Override
        public void destroy() {
            DependencyResolverProvider.INSTANCE.getAuthorizationFilter().destroy();
        }
    }

}