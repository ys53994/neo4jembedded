package us.internettechnology.neo4j.configuration;

import com.google.common.collect.ImmutableSet;
import org.neo4j.server.rest.dbms.UserService;
import org.neo4j.server.rest.management.JmxService;
import org.neo4j.server.rest.management.RootService;
import org.neo4j.server.rest.management.VersionAndEditionService;
import org.springframework.stereotype.Component;
import us.internettechnology.neo4j.util.DependencyResolverProvider;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@Component
@ApplicationPath("/db/manage")
public class Neo4jManagementApi extends Application {


    @Override
    public Set<Class<?>> getClasses() {
        return ImmutableSet.of(JmxService.class, RootService.class, VersionAndEditionService.class);
    }

    @Override
    public Set<Object> getSingletons() {
        return new HashSet<>(DependencyResolverProvider.INSTANCE.getSingletons());
    }


}