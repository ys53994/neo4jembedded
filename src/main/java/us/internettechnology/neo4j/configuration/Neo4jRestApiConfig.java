package us.internettechnology.neo4j.configuration;


import com.google.common.collect.ImmutableSet;
import org.neo4j.server.rest.web.*;
import org.springframework.stereotype.Component;
import us.internettechnology.neo4j.util.DependencyResolverProvider;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@Component
@ApplicationPath("/db/data")
public class Neo4jRestApiConfig extends Application {

    public Neo4jRestApiConfig() {
    }

    @Override
    public Set<Class<?>> getClasses() {

        return ImmutableSet.of(TransactionalService.class, RestfulGraphDatabase.class,
                DatabaseMetadataService.class,
                ExtensionService.class,
                CypherService.class,
                BatchOperationService.class,
                DatabaseMetadataService.class);
    }

    @Override
    public Set<Object> getSingletons() {
        return new HashSet<>(DependencyResolverProvider.INSTANCE.getSingletons());
    }
}