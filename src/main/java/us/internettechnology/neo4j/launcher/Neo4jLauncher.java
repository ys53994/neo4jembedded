package us.internettechnology.neo4j.launcher;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.stereotype.Component;
import us.internettechnology.neo4j.util.DependencyResolverProvider;
import us.internettechnology.neo4j.util.Neo4jUtil;

import javax.annotation.PreDestroy;
import javax.ws.rs.core.Application;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Stopwatch.createStarted;
import static java.util.Collections.emptyMap;

/**
 * Wrapper over neo4j server instance to start|stop it.
 */

@Component
public class Neo4jLauncher  {

    private static final Logger log = org.slf4j.LoggerFactory.getLogger(Neo4jLauncher.class);

    private final Neo4jServletContainerBootstrapper neo4jTomcatBootstrapper;


    private final Neo4jUtil neo4jUtil;

    @Autowired
    public Neo4jLauncher(Neo4jServletContainerBootstrapper neo4jTomcatBootstrapper, Neo4jUtil neo4jUtil) {
        this.neo4jTomcatBootstrapper = neo4jTomcatBootstrapper;
        this.neo4jUtil = neo4jUtil;
    }

    public void start() {
        final Stopwatch logTimeTracker = createStarted();
        final File neo4jWorkingDirectory = neo4jUtil.getNeo4jWorkingDir();

        log.info("Start running neo4j server ");
        log.info("Neo4j working directory is {}", neo4jWorkingDirectory.getAbsolutePath());

        final int status = neo4jTomcatBootstrapper.start(neo4jWorkingDirectory, neo4jUtil.getNeo4jConfigFile(neo4jWorkingDirectory), emptyMap());

        log.info("Neo4j server has been started. Status  is ({}). It takes {} to start", status, logTimeTracker);
    }


    public Map<String, String> getConfig() {
        if (neo4jTomcatBootstrapper.isRunning()) {
            return new HashMap<>(neo4jTomcatBootstrapper.getServer().getConfig().getRaw());
        }
        return ImmutableMap.of();
    }

    public boolean isRunning() {
        return neo4jTomcatBootstrapper.isRunning();
    }

    @PreDestroy
    public void stop() {
        if (neo4jTomcatBootstrapper.isRunning()) {
            final Stopwatch stopwatch = createStarted();
            log.info("Stopping neo4j server");
            neo4jTomcatBootstrapper.stop();
            log.info("Neo4j server has been successfully stopped . Takes {}", stopwatch);
        } else {
            log.info("Neo4j server already has been stopped.");
        }
    }



}