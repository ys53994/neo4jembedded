package us.internettechnology.neo4j.launcher;

import org.neo4j.kernel.GraphDatabaseDependencies;
import org.neo4j.kernel.configuration.Config;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.slf4j.Slf4jLogProvider;
import org.neo4j.server.CommunityBootstrapper;
import org.neo4j.server.NeoServer;

public class Neo4jServletContainerBootstrapper extends CommunityBootstrapper {


    @Override
    protected NeoServer createNeoServer(Config config, GraphDatabaseDependencies dependencies,
                                        LogProvider logProvider) {
        final LogProvider slf4jProvider = new Slf4jLogProvider();
        dependencies = dependencies.userLogProvider(slf4jProvider);
        return new EmbeddedNeo4jServer(config, dependencies, slf4jProvider);
    }
}