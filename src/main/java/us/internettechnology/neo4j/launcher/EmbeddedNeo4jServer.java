package us.internettechnology.neo4j.launcher;

import org.neo4j.graphdb.factory.GraphDatabaseSettings;
import org.neo4j.kernel.configuration.Config;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacadeFactory;
import org.neo4j.logging.LogProvider;
import org.neo4j.server.CommunityNeoServer;
import org.neo4j.server.database.Database;
import org.neo4j.server.database.InjectableProvider;
import org.neo4j.server.modules.ServerModule;
import org.neo4j.server.rest.dbms.AuthorizationDisabledFilter;
import org.neo4j.server.rest.dbms.AuthorizationEnabledFilter;
import org.neo4j.server.web.WebServer;
import org.parboiled.common.ImmutableList;
import us.internettechnology.neo4j.util.DependencyResolverProvider;
import us.internettechnology.neo4j.util.PortLessWebServer;

import javax.servlet.Filter;
import java.util.Collection;
import java.util.regex.Pattern;


public class EmbeddedNeo4jServer extends CommunityNeoServer {

    private static final Pattern[] DEFAULT_URI_WHITELIST = new Pattern[]{
            Pattern.compile("/")
    };

    public EmbeddedNeo4jServer(Config config, GraphDatabaseFacadeFactory.Dependencies dependencies, LogProvider logProvider) {
        super(config, dependencies, logProvider);

    }

    public EmbeddedNeo4jServer(Config config, Database.Factory dbFactory, GraphDatabaseFacadeFactory.Dependencies dependencies, LogProvider logProvider) {
        super(config, dbFactory, dependencies, logProvider);
    }

    protected Pattern[] getUriWhitelist() {
        return DEFAULT_URI_WHITELIST;
    }

    protected Collection<InjectableProvider<?>> createDefaultInjectables() {
        Collection<InjectableProvider<?>> singletons = super.createDefaultInjectables();
        DependencyResolverProvider.INSTANCE.setSingletons(singletons);
        return singletons;
    }

    @Override
    protected WebServer createWebServer() {
        addAuthorizationFilter();
        return new PortLessWebServer(logProvider, getConfig());
    }

    private void addAuthorizationFilter() {
        final Filter authorizationFilter;

        if (getConfig().get(GraphDatabaseSettings.auth_enabled)) {
            authorizationFilter = new AuthorizationEnabledFilter(authManagerSupplier, logProvider, DEFAULT_URI_WHITELIST);
        } else {
            authorizationFilter = new AuthorizationDisabledFilter();
        }
        DependencyResolverProvider.INSTANCE.setAuthorizationFilter(authorizationFilter);

    }

    @Override
    protected Iterable<ServerModule> createServerModules() {
        return ImmutableList.of();
    }

}
