# Embedded Neo4j Server


### Prerequisites

* JDK 1.8
* Maven 3+
* Tomcat or Jetty

### Download

* Git checkout or download as zip

### Build

Open terminal in project root folder and execute commands below to build project
By default it is building war artifact for tomcat 

``` mvn clean install ```

if you want to build artifact for jetty please add jetty profile to maven -Pjetty

``` mvn clean install -Pjetty ```

Result is **target/neo4j.war** artifact

### Deploy and Run

Copy artifact from **${project.home}/target/neo4j.war** into **${tomcat.home}/webapps** or into **${jetty.home}/webapps** .
After server has started . Neo4j should be available.

To check open http://youhost:7474/ - will opened standard neo4j web console

By default neo4j will be exploded into ${servlet.container.home}/

**${servlet.container.home}/data** - neo4j data dir

**${servlet.container.home}/conf/neo4j.conf** - configuration file will be created

**${serlvet.container.home}/logs/**  - neo4j configuration



### Manage Embedded Neo4j Instance
Have added ability (rest endpoint) to manage neo4j via rest api

if configuration option *neo4j.embedded.management.controller.enabled* set as true (default) then managment controller will be availabe.
This controller gives ability to start|stop|get info about configuration|status without whole servlet container restarting


``` ${server.host}:${server:port}/neo4j/management/restart``` - restart neo4j

``` ${server.host}:${server:port}/neo4j/management/stop``` - stop neo4j

``` ${server.host}:${server:port}/neo4j/management/start``` - start neo4j

``` ${server.host}:${server:port}/neo4j/management/status``` - show status it is running or not

``` ${server.host}:${server:port}/neo4j/management/configuration``` - print neo4j configuration


If  *neo4j.embedded.management.controller.enabled* set as false then embedded neo4j will started when server started during exploding *.war
and stopped when server stopped


### Main Configuration
Application configuration is located into **src/main/resources/application.properties** . Configuration can be override via configuration file
or via java jvm **-D** properties


#### Configuration options

**neo4j.embedded.working.dir**  - by default it is empty and application is getting JETTY_HOME or CATALINA_HOME as working dir for neo4j

**neo4j.embedded.working.subdir** - by default it is empty and application explode neo4j configuration into ${server.home}/ 
if you want for example that all neo4j files located into separate subfolder change this property. For example if set to **neo4j**
then all neo4j configuration and files will be located under ${server.home}/neo4j

**neo4j.embedded.config.file.location** - Path to neo4j neo4j.conf file .Configuration path by default is empty during start copied default configuration into ${neo4j.embedded.home}/conf/neo4j.conf

**neo4j.embedded.management.controller.enabled**  - by default it is true . If it is true then profile REST API for neo4j management. If it is false then REST API not available.



### Neo4j Configuration

Embedded neo4j is supported all features as standalone instance. And these features can be configured via neo4j.conf


### Development

#### How to rebuild UI ( neo-browser ) 

 1. Download sources from public repository -  https://github.com/neo4j/neo4j-browser/
 2. Follow instruction in  https://github.com/neo4j/neo4j-browser/blob/master/README.md how to build sources
 3. After sources have been builded replace all/copy content from ${source.home}/dist/* into ${project.home}\src\resources\static\*
 4. Rebuild neo4jembedded project or you can copy sources in deployed artifact under tomcat
 
 
### How embedded neo4j is working
 
##### 1. Neo4j Version
Used sources of version 3.4 ( maven dependencies ) they are included into pom.xml


##### 2. Sources which has been modified with commentaries
 
 Neo4j sources wasn't modified . Basically I have override some neo4j classes to prevent starting inner jetty container which provide REST API and filters
 and expose these REST API over current application. Below is descriptions of added classes and what they are doing.
 
 ***us.internettechnology.neo4j.Neo4jApplication*** - standard spring boot servlet container launcher.
 
 ***us.internettechnology.neo4j.launcher.EmbeddedNeo4jServer*** - it is start point . This class is responsible for embedded neo4j configuration
 
 ***us.internettechnology.neo4j.listener.WebContainerLifecycleListener*** - Servlet Listener used as entry point to stop neo4j database on _**contextInitialized**_ and stop neo4j on _**contextDestroyed**_
 
 ***us.internettechnology.neo4j.launcher.Neo4jServletContainerBootstrapper*** - Just override inner CommunityBootstrapper . Reason is add logging to same stream output as tomcat
 
 ***us.internettechnology.neo4j.launcher.Neo4jLauncher*** - wrapper class to start/stop/get status from neo4j server
 
 ***us.internettechnology.neo4j.launcher.EmbeddedNeo4jServer*** - override neo4j server . ( Avoid to start build-in jetty and collect neo4j dependencies to use them in API)
 
 ***us.internettechnology.neo4j.configuration.Neo4jRestApiConfig*** - spring configuration for Neo4j rest api
  
 ***us.internettechnology.neo4j.configuration.Neo4jManagementApi*** - spring configuration for Neo4j management rest api
   
 ***us.internettechnology.neo4j.configuration.Neo4jUserApiConfig*** - spring configuration for Neo4j user rest api
 
 ***us.internettechnology.neo4j.util.Neo4jUtil*** - utility class which contains utility methods to work with configuration ( copy / found)
 
 
 
##### How to update neo4j version
 
 In **${project.home}/pom.xml** maven just change version in properties as shown below
        
         ```<neo4j.version>3.4.0</neo4j.version>```


  
##### How to rebuild project
 
 
 
   To rebuild project just invoke maven 
   
   ```mvn clean install```
   
   
   
   
##### How to  deploy project into tomcat
 
  If previous version was deployed on tomcat then better to clean all tomcat caches
  
  _remove ${tomcat.home}/temp_
  
  _remove ${tomcat.home}/work_
  
  _remove ${tomcat.home}/webapp/neo4j_  - exploded artifact
  
  
  Copy new artifact into project
  
  copy **${project.home}/target/neo4j.war** into **${tomcat.home}/webapps**
  
  
  Start tomcat
  
  
##### How to change icons in neo4j-browser

Neo4j-browser is using build-in icon set to change icon you have to modify source code. Link below is main class which is responsible for icons. Modify it ( override css styles) and rebuild client 

https://github.com/neo4j/neo4j-browser/blob/master/src/browser/components/icons/Icons.jsx






 
 
 
 
 
 
  








 
 
 









